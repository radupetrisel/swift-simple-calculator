//
//  Equals.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 19.02.2023.
//

import Foundation

struct Equals : Sign {
    var value = "="
    
    func reduce(_ operation: some Operation) -> Int {
        switch operation {
        case let operation as IntOperation:
            return operation.value
        case let operation as BinaryOperation:
            return operation.sign.reduce(operation)
        default:
            return 0
        }
    }
}
