//
//  Sign.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 18.02.2023.
//

import Foundation

protocol Sign {
    var value: String { get }
    
    func reduce(_ operation: some Operation) -> Int
}

func getSign(_ value: String) -> Sign {
    switch value {
    case "+":
        return Plus()
    case "-":
        return Minus()
    case "x":
        return Times()
    case "/":
        return Divide()
    default:
        return Equals()
    }
}
