//
//  Minus.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 19.02.2023.
//

import Foundation

struct Minus : Sign {
    var value = "-"
    
    func reduce(_ operation: some Operation) -> Int {
        switch operation {
        case let operation as IntOperation:
            return operation.value
        case let operation as BinaryOperation where operation.sign is Minus:
            return operation.left - (operation.right ?? 0)
        default:
            return 0
        }
    }
}
