//
//  Divide.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 19.02.2023.
//

import Foundation

struct Divide : Sign {
    var value = "/"
    
    func reduce(_ operation: some Operation) -> Int {
        switch operation {
        case let operation as IntOperation:
            return operation.value
        case let operation as BinaryOperation where operation.right != nil && operation.right != 0:
            return operation.left / operation.right!
        default:
            return 0
        }
    }
}
