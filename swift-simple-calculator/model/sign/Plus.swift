//
//  Plus.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 18.02.2023.
//

import Foundation

struct Plus : Sign {
    var value = "+"
    
    func reduce(_ operation: some Operation) -> Int {
        switch operation {
        case let operation as IntOperation:
            return operation.value
        case let operation as BinaryOperation where operation.sign is Plus:
            return operation.left + (operation.right ?? 0)
        default:
            return 0
        }
    }
}
