//
//  Times.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 19.02.2023.
//

import Foundation

struct Times : Sign {
    var value = "x"
    
    func reduce(_ operation: some Operation) -> Int {
        switch operation {
        case let operation as IntOperation:
            return operation.value
        case let operation as BinaryOperation:
            return operation.left * (operation.right ?? 1)
        default:
            return 0
        }
    }
}
