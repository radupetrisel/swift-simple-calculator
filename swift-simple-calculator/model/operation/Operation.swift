//
//  Operation.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 18.02.2023.
//

import Foundation

protocol Operation {
    associatedtype T
    
    var value: T { get }
    var displayValue: String { get }
    
    func apply(_ operation: some Operation) -> any Operation
    
    func apply(_ sign: Sign) -> any Operation
}

struct Empty : Operation {
    typealias T = Int
    
    var value = 0
    var displayValue = "0"
    
    func apply(_ operation: some Operation) -> any Operation {
        return operation
    }
    
    func apply(_ sign: Sign) -> any Operation {
        return BinaryOperation(value: self.value, sign: sign)
    }
}
