//
//  IntOperation.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 18.02.2023.
//

import Foundation

struct IntOperation : Operation {
    typealias T = Int
    
    var value: Int
    var displayValue: String { "\(value)" }
    
    func apply(_ operation: some Operation) -> any Operation {
        switch operation {
        case let operation as IntOperation:
            return IntOperation(value: self.value * 10 + operation.value)
        default:
            return self
        }
    }
    
    func apply(_ sign: Sign) -> any Operation {
        return BinaryOperation(value: self.value, sign: sign)
    }
}
