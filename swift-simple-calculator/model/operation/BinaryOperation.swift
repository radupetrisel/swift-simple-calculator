//
//  Addition.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 18.02.2023.
//

import Foundation

struct BinaryOperation : Operation {
    typealias T = (left: Int, right: Int?)
    
    var value: (left: Int, right: Int?)
    let sign: Sign
    
    var displayValue: String {
        "\(value.left)" + "\(sign.value)" + value.right.toString()
    }
    
    var left: Int { value.left }
    var right: Int? { value.right }
    
    init(left: Int, right: Int?, sign: Sign) {
        self.value = (left, right)
        self.sign = sign
    }
    
    init(value: Int, sign: Sign) {
        self.init(left: value, right: nil, sign: sign)
    }
    
    func apply(_ operation: some Operation) -> any Operation {
        switch operation {
        case let operation as IntOperation:
            return BinaryOperation(left: left, right: (right ?? 0) * 10 + operation.value, sign: self.sign)
        default:
            return self
        }
    }
    
    func apply(_ sign: Sign) -> any Operation {
        let result = self.sign.reduce(self)
        
        if sign is Equals {
            return IntOperation(value: result)
        }
        
        return BinaryOperation(value: result, sign: sign)
    }
}

fileprivate extension Int? {
    func toString() -> String {
        return self != nil ? "\(self!)" : ""
    }
}
