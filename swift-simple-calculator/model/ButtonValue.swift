//
//  ButtonValue.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 14.02.2023.
//

import Foundation

enum ButtonValue : Hashable, Identifiable {
    case numeric(value: Int)
    case sign(sign: String)
    case special(value: String)
    
    var id: Self {
        return self
    }
    
    var pretty: String {
        switch self {
        case .numeric(let value):
            return String(value)
        case .sign(let sign):
            return sign
        case .special(let value):
            return value
        }
    }
}
