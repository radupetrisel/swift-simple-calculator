//
//  ArrayExtensions.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 16.02.2023.
//

import Foundation

extension Array: Identifiable where Element: Hashable {
    public var id: Int { self.hashValue }
}
