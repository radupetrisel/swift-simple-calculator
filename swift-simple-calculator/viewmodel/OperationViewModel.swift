//
//  OperationViewModel.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 16.02.2023.
//

import Foundation
import Combine

class OperationViewModel : ObservableObject {
    @Published var operation : any Operation = Empty()
    @Published var displayValue = ""
    
    private var subscriptions: [AnyCancellable] = []
    
    init() {
        $operation
            .map { op in op.displayValue }
            .assign(to: \.displayValue, on: self)
            .store(in: &subscriptions)
    }
    
    deinit {
        subscriptions.cancel()
    }
}

private extension Array where Element : AnyCancellable {
    func cancel() {
        self.forEach { cancellable in
            cancellable.cancel()
        }
    }
}
