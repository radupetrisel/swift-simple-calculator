//
//  ContentView.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 14.02.2023.
//

import SwiftUI

struct ContentView: View {
    private let buttonsMatrix: [[ButtonValue]] = [
        [.numeric(value: 7), .numeric(value: 8), .numeric(value: 9), .sign(sign: "+")],
        [.numeric(value: 4), .numeric(value: 5), .numeric(value: 6), .sign(sign: "-")],
        [.numeric(value: 1), .numeric(value: 2), .numeric(value: 3), .sign(sign: "x")],
        [.special(value: "AC"), .numeric(value: 0), .sign(sign: "="), .sign(sign: "/")]
    ]
    
    @EnvironmentObject private var operationViewModel: OperationViewModel
    
    var body: some View {
        VStack {
            Spacer()
            
            Text(operationViewModel.displayValue)
                .font(.largeTitle)
                .frame(maxWidth: .infinity, alignment: .trailing)
            
            ForEach(buttonsMatrix) { row in
                ButtonRow(buttons: row)
                    .environmentObject(operationViewModel)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(OperationViewModel())
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}
