//
//  ButtonRow.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 16.02.2023.
//

import SwiftUI
import Combine

struct ButtonRow: View {
    @EnvironmentObject private var operationViewModel: OperationViewModel
    let buttons: [ButtonValue]
    
    var body: some View {
        HStack {
            ForEach(buttons) { button in
                switch button {
                case .numeric(let value):
                    numericButton(from: value)
                case .sign(let sign):
                    signButton(sign: sign)
                case .special(let value):
                    specialButton(value: value)
                }
            }
        }
    }
    
    private func numericButton(from value: Int) -> CalculatorButton {
        return CalculatorButton(value: .numeric(value: value)) {
            operationViewModel.operation = operationViewModel.operation.apply(IntOperation(value: value))
        }
    }
    
    private func signButton(sign: String) -> CalculatorButton {
        return CalculatorButton(value: .sign(sign: sign)) {
            operationViewModel.operation = operationViewModel.operation.apply(getSign(sign))
        }
    }
    
    private func specialButton(value: String) -> CalculatorButton {
        return CalculatorButton(value: .special(value: value)) {
            operationViewModel.operation = Empty()
        }
    }
}

struct ButtonRow_Previews: PreviewProvider {
    static var previews: some View {
        ButtonRow(buttons: [.numeric(value: 1), .sign(sign: "+"), .special(value: ".")])
            .environmentObject(OperationViewModel())
    }
}
