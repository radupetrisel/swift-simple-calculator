//
//  CalculatorButton.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 14.02.2023.
//

import SwiftUI

struct CalculatorButton: View {
    let value: ButtonValue
    let callback: () -> Void
    
    var body: some View {
        Button(value.pretty) {
            callback()
        }
        .frame(minWidth: 50, maxWidth: 100, minHeight: 50, maxHeight: 100)
        .foregroundColor(.black)
        .background(.gray)
        .clipShape(Circle(), style: FillStyle())
    }
}

struct CalculatorButton_Previews: PreviewProvider {
    static var previews: some View {
        CalculatorButton(value: ButtonValue.numeric(value: 1)) { }
    }
}
