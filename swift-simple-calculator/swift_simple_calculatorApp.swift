//
//  swift_simple_calculatorApp.swift
//  swift-simple-calculator
//
//  Created by Radu Petrisel on 14.02.2023.
//

import SwiftUI

@main
struct swift_simple_calculatorApp: App {
    let operationViewModel = OperationViewModel()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(operationViewModel)
        }
    }
}
